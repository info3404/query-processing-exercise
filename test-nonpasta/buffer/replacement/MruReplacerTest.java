package buffer.replacement;


import buffer.BufferFrame;
import buffer.replacement.MruReplacer;
import buffer.replacement.Replacer;
import disk.Page;
import disk.PageId;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit tests for practice task
 * To get these tests to pass you will need to fully implement MruReplacementPolicy
 * A solution for this task is included within next week's tasks
 * (@see MruReplacementPolicy)
 */
public class MruReplacerTest {

    private static final int NUMFRAMES = 5;
	private Replacer mReplacementPolicy;
    private List<BufferFrame> mPool;
    private BufferFrame A, B, C, D, E;

	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000); 
    
    @Before
    public void setUp() throws Exception {
        mReplacementPolicy = new MruReplacer();
        mPool = generateBufferFrameList(NUMFRAMES);
        A = mPool.get(0);
        B = mPool.get(1);
        C = mPool.get(2);
        D = mPool.get(3);
        E = mPool.get(4);
    }

    @Test
    public void testChooseWhenOneFrameUnused() throws Exception {
        mReplacementPolicy.notify(mPool, A);
        mReplacementPolicy.notify(mPool, B);
        mReplacementPolicy.notify(mPool, D);
        mReplacementPolicy.notify(mPool, E);
        // Test 4/5 used, but one never used
        assertEquals("Should get most recently used page even when another page is unused",E, mReplacementPolicy.choose(mPool));
    }

    @Test
    public void testChooseNormal() throws Exception {
        mReplacementPolicy.notify(mPool, A);
        mReplacementPolicy.notify(mPool, B);
        mReplacementPolicy.notify(mPool, D);
        mReplacementPolicy.notify(mPool, E);
        mReplacementPolicy.notify(mPool, C);
        // Test that oldest frame is chosen
        assertEquals("Should get most recently used page when all pages used equally",C, mReplacementPolicy.choose(mPool));
    }

    @Test
    public void testChooseOutOfOrder() throws Exception {
        mReplacementPolicy.notify(mPool, A);
        mReplacementPolicy.notify(mPool, B);
        mReplacementPolicy.notify(mPool, A);
        mReplacementPolicy.notify(mPool, D);
        mReplacementPolicy.notify(mPool, E);
        mReplacementPolicy.notify(mPool, C);
        // Test that oldest frame is chosen
        assertEquals("Should get most recently used page even when other pages are used more frequently",C, mReplacementPolicy.choose(mPool));
    }

    @After
    public void testNoChangeToList() {
        assertEquals("Pool size should not have changed",NUMFRAMES, mPool.size());
    }
    
	/**
	 * Create a buffer pool with a specified number of frames and
	 * populate each with a page.
	 * @param numFrames
	 * @return
	 */
    private static List<BufferFrame> generateBufferFrameList(int numFrames) {
        List<BufferFrame> pool = new LinkedList<>();
        for(int i = 0; i < numFrames; i++) {
            BufferFrame temp = new BufferFrame();
            try {
                temp.setPage(new PageId(i), new Page());
            } catch (BufferFrame.BufferFrameDirtyException e) {}
            pool.add(temp);
        }
        return pool;
    }

    /**
     * Call the replacer's notify method multiple times on frames of a pool
     * @param replacer Replacer to call
     * @param pool Buffer pool
     * @param timesToNotify Number of times to call notify() on each frame
     */
	private static void notifyMany(Replacer replacer, List<BufferFrame> pool, int... timesToNotify) {
        for(int i = 0; i < timesToNotify.length; i++) {
            int notify = timesToNotify[i];
            for(int j = 0; j < notify; j++) {
                replacer.notify(pool, pool.get(i));
            }
        }
    }

	private static void simulatePageLoad(Replacer replacer, List<BufferFrame> pool, BufferFrame frame, int pageId) {
		frame.setPage(new PageId(pageId), new Page());
		replacer.notify(pool, frame);
	}

}