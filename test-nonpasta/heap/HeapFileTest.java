package heap;

import access.read.AccessIterator;
import access.write.AccessInserter;
import disk.PageId;
import global.DatabaseManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import static org.junit.Assert.*;

public class HeapFileTest {
	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    private HeapFile student;
    private DatabaseManager dbms;

    @Before
    public void setUp() throws Exception {
    	dbms = new DatabaseManager();
    	
        // Create Test Schema
        TupleDesc studentSchema = new TupleDesc();
        studentSchema.addString("name").addInteger("age").addDouble("speed").addBoolean("male");
        dbms.getCatalog().addSchema(studentSchema, "students");
        student = dbms.getHeapFile("students");
        try(AccessInserter inserter = student.inserter()) {
            for (int i = 0; i < 1000; i++) {
                inserter.insert("Michael", i / 2, 200.0 - i, true);
            }
        }
    }

    @Test
    public void testInsertRecord() throws Exception {
        int i = 0;
        AccessIterator studentIterator = student.iterator();
        while(studentIterator.hasNext()) {
            Tuple studentTuple = studentIterator.next();
            assertTrue(studentTuple.rowEquals("Michael", i/2, 200.0 - i, true));
            i++;
        }
        assertEquals(1000, i);
    }

    @Test
    public void testNoUnpinnedPages() throws Exception {
        int i = 0;
        AccessIterator studentIterator = student.iterator();
        while(studentIterator.hasNext()) {
            studentIterator.next();
            i++;
        }
        assertEquals(1000, i);
        studentIterator.close();
        assertEquals(0, dbms.getBufferManager().getNumberOfPinnedPages());
        PageId cheatTest = dbms.getBufferManager().getNewPage();
        dbms.getBufferManager().getPage(cheatTest);
        assertNotEquals(0, dbms.getBufferManager().getNumberOfPinnedPages());
    }

    @Test
    public void testTupleReturnWithPageIdSet() throws Exception {
        int i = 0;
        AccessIterator studentIterator = student.iterator();
        while(studentIterator.hasNext()) {
            assertTrue(studentIterator.next().getPageId().isValid());
        }
    }

    @After
    public void tearDown() throws Exception {
    	dbms.close();
    }
}