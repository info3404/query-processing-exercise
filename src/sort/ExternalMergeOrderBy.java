package sort;

import static global.DatabaseConstants.MERGE_SORT_BUFFER_FRAMES;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import access.read.AccessIterator;
import access.write.AccessInserter;
import buffer.BufferManager.BufferAccessException;
import global.DatabaseManager;
import heap.HeapFile;
import heap.HeapPage;
import heap.Tuple;
import heap.TupleDesc;

/**
 * Implementation of an External Merge Sort
 * General usage is
 * 
 * String[] sortColumns = new String{"firstcol", "secondCol"};
 * ExternalMergeOrderBy sorter = ExternalMergeOrderBy(dbms, sortColumns);
 * HeapFile relation = dbms.getHeapFile("relationname");
 * AccessIterator rows = relation.iterator()
 * AccessIterator sortedRows sorter.getSortedRows(rows);
 * rows.close()
 * 
 * Implement this for Task 3 (HARD)
 * - You will need to implement the first round (sorted runs phase)
 * - You will then need to do a multiway merge to merge (M-1) runs together
 * - Once there is only one run left, provide access to iterate over this
 *
 * Note: Since AccessIterator only supports hasNext() and next(), we implement peeking functionality by creating
 *       an array to store the topmost value, referred to as peekedTuples in the methods below.
 *
 * References:
 * - The lecture slides provide a good reference for the method involved.
 * - The lecture notes found here: http://people.cs.aau.dk/~simas/aalg04/esort.pdf are also a good reference point
 */
public class ExternalMergeOrderBy  {

    private Comparator<Tuple> tupleComparator;
	private DatabaseManager dbms;

    /**
     * Constructor for the ExternalMergeOrderBy
     * - Once this constructor is finished, call `getSortedRows(rows)` to get an AccessIterator
     *   over a list of sorted elements
     * - Thus you will need to do your sort & merge here
     * @param rows
     * @param columns
     * @throws BufferAccessException 
     */
    public ExternalMergeOrderBy(DatabaseManager dbms, String[] sortColumns) {
        this.dbms = dbms;
        tupleComparator = new ColumnComparator(sortColumns);
        if(MERGE_SORT_BUFFER_FRAMES < 3) throw new IllegalStateException("Sort buffer too small to perform merges"); 

	}

	/**
	 * Performs the sort and merge steps repeatedly until a single sorted file 
	 * is generated
	 * @param rows
	 * @return iterator for sorted file
	 * @throws BufferAccessException
	 */
	public AccessIterator getSortedRows(AccessIterator rows) throws BufferAccessException {
		List<HeapFile> runs = produceSortedRuns(rows);
		
		// TODO by student 
		// repeatedly merge runs with mergeSortedRuns until a single sorted run is produced

		return runs.get(0).iterator();
	}


	/**
     * Reads all pages into memory and divides them into groups of B (MERGE_SORT_BUFFER_FRAMES) pages.
     * Each of these groups is sorted (in memory) and then written to disk. (though do one at a time)
     * Since we don't know the number of records that we have in total, we can do some crafty mathematics
     * to work out the maximum number of tuples that we can fit in B pages.
     *
     * You will want to create a new file to store this, try using:
     * `HeapFile file = dbms.getTempHeapFile(schema);`
     * to create the file with the appropriate schema of the iterator
     * To simplify matters, we will store the sorted blocks in a HeapFile.
     *
     * To insert records into this file, try using:
     *      try(AccessInserter fileInserter = file.inserter()) {
     *          fileInserter.insert(...next tuple to insert...);
     *`     }
     *
     *
     * @param rows iterator over a whole relation/table
     * @return list of sorted runs
     * @throws BufferAccessException 
     */
    protected List<HeapFile> produceSortedRuns(AccessIterator rows) throws BufferAccessException {
    	TupleDesc schema = rows.getSchema();
        
    	/**
    	 * Maximum number of tuples that can be stored in a sorted run
    	 * in this method.
    	 */ 
        int tuplesPerRun =  HeapPage.getMaxRecordsOnPage(schema)  * MERGE_SORT_BUFFER_FRAMES;
        
        /**
         * Structure to hold tuples temporarily in memory (slightly cheating, 
         * should really work directly within the buffered pages, but that's
         * more complicated)
         */ 
        List<Tuple> tuples = new ArrayList<Tuple>();
        
        /**
         * List of HeapFiles of sorted runs
         */ 
        List<HeapFile> blocks = new ArrayList<HeapFile>();
        
        // Iterate over each row to produce the sorted runs
        while(rows.hasNext()) {
            // @TODO by student
        }
        return blocks;
    }

    /**
     * Merges groups of (M-1) HeapFiles together.
     * You should take the List of runs, divide them into (M-1) groups, and perform a multi-way merge on each group
     * For each of these groups, you should write the merged result out to file as you go.
     *
     * M is the MERGE_SORT_BUFFER_FRAMES variable
     *
     * You will want to create a new file to store this, try using:
     * `HeapFile mergedFile = dbms.getTempHeapFile(schema);`
     * to create the file with the appropriate schema of the iterator
     *
     * To insert records into this file, try using:
     *      try(AccessInserter mergedFileInserter = mergedFile.inserter()) {
     *          mergedFileInserter.insert(...next tuple to insert...);
     *`     }
     *
     * Look at using pickNextTuple as a way to find the next tuple to add.
     * You will need to use this as part of multi-way merging.
     *
     * Also! Since there is no peek functionality in an AccessIterator, you will need to cache to topmost elements of each
     * AccessIterator in a Tuple[]
     *
     * @param runs the full list of runs from the previous round
     * @return a list of sorted heapfiles, merged in the method
     * @throws BufferAccessException 
     */
    protected List<HeapFile> mergeSortedRuns(List<HeapFile> runs) throws BufferAccessException {
        if(runs.size() < 2) return runs;
        TupleDesc schema = runs.get(0).getSchema();  // Assume all runs have the same schema

        // @TODO by student
        
        return runs;
    }

    /**
     * Utility method to find the next tuple to be merged. 
     * Finds the minimum tuple out of all the current sorted iterators.
     * Since we know these iterators are sorted, this tuple should be added to the end of our new file once we return
     * @param unmergedRuns the list of access iterators for this run
     * @param peekedTuples array of tuples representing the topmost elements of the access iterators
     */
    private Tuple pickNextTuple(AccessIterator[] unmergedRuns, Tuple[] peekedTuples) {
        int index = findMinimum(peekedTuples);
        Tuple nextTuple = peekedTuples[index];
        // Replaces the tuple that we've just removed with the next one from the iterator if it has another value
        if (unmergedRuns[index].hasNext()) {
            peekedTuples[index] = unmergedRuns[index].next();
        }
        else {
            peekedTuples[index] = null;
        }
        return nextTuple;
    }

    /**
     * Utility method to checks if there is another tuple to be added / merged. 
     * If there is, then we need to continue so we can add this
     * to the merged run.
     */
    private boolean hasNextInList(Tuple[] peekedTuplesList) {
        for(Tuple i : peekedTuplesList) {
            if(i != null)
                return true;
        }
        return false;
    }

    /**
     * Utility method to find the minimum / next tuple in a list of tuples to return.
     * Assumes null entries are exhausted iterators and ignores them; returns the next minimum tuple;
     */
    private int findMinimum(Tuple[] current) {
        int i = 0;
        while(i < current.length && current[i] == null) {i++;}
        int minimum = i++;
        for(; i < current.length; i++) {
            if(current[i] == null) continue;
            if(tupleComparator.compare(current[i], current[minimum]) < 0) {
                minimum = i;
            }
        }
        return minimum;
    }
}
