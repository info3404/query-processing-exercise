package sort;

import access.read.AccessIterator;
import heap.Tuple;
import heap.TupleDesc;

import java.util.*;

/**
 * In-memory Sorting of the Tuples for the OrderBy Operation
 * - Loads all of the tuples into memory, and then sorts this list
 * - Returns the sorted tuples in order when the iterator is accessed
 */
public class InMemoryOrderBy extends AccessIterator {

    private TupleDesc schema;

    /**
     * Constructor for the in-memory OrderBy sort iterator
     * - You will need to sort the tuples in the constructor
     * - Look at using `new ColumnComparator(columns)` and Collections.sort(...)
     * @param rows iterator over the rows to sort
     * @param columns the columns to sort on
     */
    public InMemoryOrderBy(AccessIterator rows, final String[] columns) {
        schema = rows.getSchema();
    	// @TODO by student
    }

    @Override
    public void close() {
    	// @TODO by student
    }

    @Override
    public TupleDesc getSchema() {
        return schema;
    }

    @Override
    public boolean hasNext() {
    	// @TODO by student
        return false;
    }

    @Override
    public Tuple next() {
    	// @TODO by student
        return null;
    }

}
