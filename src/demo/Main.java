package demo;

import java.io.IOException;

import access.write.AccessInserter;
import buffer.BufferManager.BufferAccessException;
import execution.QueryEngine;
import global.DatabaseManager;
import heap.HeapFile;
import heap.TupleDesc;

/**
 * Example Execution using Database Components.
 * See the test cases for more examples
 * Here a few to get you started to play around:
 * SELECT name, age FROM students WHERE age < 10;
 * SELECT name, speed FROM students WHERE speed > 10 ORDER BY speed, name;
 */
public class Main {

    public static void main(String[] args) throws IOException, BufferAccessException {
        DatabaseManager dbms = new DatabaseManager();

        // Create the Schema
        TupleDesc studentSchema = new TupleDesc()
                .addString("name").addInteger("age").addDouble("speed").addBoolean("male");

        // Add Schema to the Catalog
        dbms.getCatalog().addSchema(studentSchema, "students");

        // Create a new heap file
        HeapFile students = dbms.getHeapFile("students");
        AccessInserter efficientStudentInsert = students.inserter();
        // Inserts some records into the heap file
        for(int i = 0; i < 10000; i++) {
            efficientStudentInsert.insert("Michael", i*100, Math.random()*100, true);
            efficientStudentInsert.insert("Rachael", i*2,  Math.random()*100, false);
        }
        efficientStudentInsert.close();
        QueryEngine queryEngine = new QueryEngine(dbms);
        queryEngine.run();
        dbms.getBufferManager().flushDirty();
    }

}
