package global;

import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import buffer.replacement.*;
import disk.DiskManager;
import heap.HeapFile;
import heap.TupleDesc;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Database Components are initialised and stored in this class.
 */
public class DatabaseManager {

    private boolean isInitialised;
    private DiskManager dm;
    private BufferManager bm;
    private Catalog catalog;

    public DatabaseManager() {
    	initialiseComponents();
    }
    
    private void initialiseComponents() {
        if(!isInitialised) {
            try {
            	File dbFile = File.createTempFile(DatabaseConstants.DEFAULT_DB_NAME,".tmp");
        		dbFile.deleteOnExit();
        		dm = new DiskManager(DatabaseConstants.DEFAULT_DB_NAME,1, new RandomAccessFile(dbFile,"rw"));
            } catch (IOException e) {}
            bm = new BufferManager(DatabaseConstants.MAX_BUFFER_FRAMES, new MruReplacer(), dm); // Single frame to force replacement for every page
            catalog = new Catalog();
            isInitialised = true;
        }
        // Do nothing
    }

    public void resetComponents() {
        isInitialised = false;
        initialiseComponents();
    }

    public Catalog getCatalog() {
        if(catalog == null) {
            throw new ComponentsNotInitialisedError();
        }
        return catalog;
    }

    public DiskManager getDiskManager() {
        if(dm == null) {
            throw new ComponentsNotInitialisedError();
        }
        return dm;
    }

    public BufferManager getBufferManager() {
        if(bm == null) {
            throw new ComponentsNotInitialisedError();
        }
        return bm;
    }

    public static class ComponentsNotInitialisedError extends Error {}

	public void close() {
		try {
			bm.flushDirty();
		} catch (BufferAccessException e) {
			e.printStackTrace();
		}
		try {
			dm.reset();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public HeapFile getHeapFile(String name) throws BufferAccessException {
		TupleDesc schema = catalog.readSchema(name); 
		return new HeapFile(schema, name, bm);
	}

	public HeapFile getTempHeapFile(TupleDesc schema) throws BufferAccessException {
        return new HeapFile(schema, bm);
    }
}
