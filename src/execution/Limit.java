package execution;

import access.read.AccessIterator;
import filter.Filter;
import heap.Tuple;
import heap.TupleDesc;

/**
 * Class to restrict the number of tuples that are output.
 * - We will accept a tuple (match its condition), if we haven't already matched more than 'limit' tuples
 * - Once we have accepted 'limit' tuples, we should stop searching
 */
public class Limit extends AccessIterator {

    private AccessIterator rows;

    public Limit(AccessIterator rows, int limit) {
        // @TODO by student
        throw new UnsupportedOperationException();
    }

    @Override
    public void close() {
        rows.close();
    }

    @Override
    public TupleDesc getSchema() {
        return rows.getSchema();
    }

    @Override
    public boolean hasNext() {
    	// @TODO by student
        return false;
    }

    @Override
    public Tuple next() {
    	// @TODO by student
        return null;
    }
}
