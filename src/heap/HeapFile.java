package heap;

import access.read.AccessIterator;
import access.read.HeapFileIterator;
import access.write.AccessInserter;
import access.write.HeapFileInserter;
import buffer.BufferManager;
import buffer.BufferManager.BufferAccessException;
import disk.HeaderPage;
import disk.PageId;
import global.DatabaseConstants;

/**
 * Represents a collection of unordered pages containing tuples. (A collection of HeapPages)
 * - Relies on a schema to interpret the Tuples
 */
public class HeapFile {

	protected TupleDesc schema; // All pages in file use this schema
	protected PageId firstPageId; // Starting page for file
	protected BufferManager bufferManager; // Handles access to pages
	protected String relationName; // ID stored in each page to identify its owning relation

	/**
	 * Initialises the HeapFile with the given schema and using a specified 
	 * buffer manager. 
	 * @param schema
	 * @param bm
	 * @throws BufferAccessException 
	 */
	public HeapFile(TupleDesc schema, String relationName, BufferManager bm) throws BufferAccessException {
		this.schema = schema;
		this.relationName = relationName;
		bufferManager = bm;
		
		firstPageId = HeaderPage.getFileEntry(bufferManager, relationName);
		// If we haven't created the HeapFile for this schema yet, then do so
		if(!firstPageId.isValid()) {
			firstPageId = bufferManager.getNewPage();
			HeaderPage.setFileEntry(bufferManager, relationName, firstPageId);
			HeapPage firstPage = new HeapPage(bufferManager.getPage(firstPageId), schema);
			firstPage.initialise(relationName);
			bufferManager.unpin(firstPageId, true);
		}
	}
	
    /**
     * Creates a temporary HeapFile with the given schema
     * Note: this is created in our database. As such, over time there will be empty pages
     * that accumulate due to the lack of deletion in the system.
     * @throws BufferAccessException 
     */
    public HeapFile(TupleDesc schema, BufferManager bm) throws BufferAccessException {
		String ts = Long.toString(System.currentTimeMillis());
		String rnd = Integer.toString( (100 + (int)(100.0 * Math.random()) % 100)); 
		this.relationName = "tmp" + ts.substring(Math.max(0, ts.length() - DatabaseConstants.MAX_TABLE_NAME_LENGTH - 6), ts.length());
		this.schema = schema;
		bufferManager = bm;

        firstPageId = bm.getNewPage();
        HeapPage firstPage = new HeapPage(bm.getPage(firstPageId), schema);
        firstPage.initialise(relationName);
        bm.unpin(firstPageId, true);
    }

    public AccessIterator iterator() throws BufferAccessException {
        return new HeapFileIterator(firstPageId, bufferManager, schema);
    }

    public AccessInserter inserter() throws BufferAccessException {
        return new HeapFileInserter(firstPageId, bufferManager, schema);
    }

    public TupleDesc getSchema() {
    	return schema;
    }

	public String printStats() {
		return "Relation " + this.relationName + ", firstPageId" + this.firstPageId.get() + ", schema " + this.schema.hashCode();
	}
}
