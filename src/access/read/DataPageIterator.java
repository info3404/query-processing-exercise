package access.read;

import java.util.Iterator;

import disk.DataPage;
import heap.Tuple;
import heap.TupleDesc;

/**
 * Provides an iterator for the tuples on a page
 */
public class DataPageIterator implements Iterator<Tuple> {

    private DataPage dataPage;
    private int slotNo;
	private TupleDesc schema;

    /**
     * Takes a data page and initialises the current slot as 0
     * @param dataPage the page to iterate over
     * @param schema 
     */
    public DataPageIterator(DataPage dataPage, TupleDesc schema) {
    	this.schema = schema;
        this.dataPage = dataPage;
        this.slotNo = 0;
    }

    /**
     * Whether there is another record on the page to read
     * @return true if current slot number is still within the count on the record
     */
    @Override
    public boolean hasNext() {
        return slotNo < dataPage.getRecordCount();
    }

    /**
     * Returns the next tuple and increments the slot counter
     * @return the next tuple on the page
     */
    @Override
    public Tuple next() {
    	// TODO: Get the tuple schema from the page
        Tuple newTuple = new Tuple(schema);
        dataPage.getRecord(slotNo, newTuple);
        slotNo++;
        return newTuple;
    }

    /**
     * Get next value without progressing the iterator
     * @return
     */
	public Tuple peekNext() {
        Tuple newTuple = new Tuple(schema);
        dataPage.getRecord(slotNo, newTuple);
        return newTuple;
	}

    /**
     * Unsupported remove operation. Ignore this.
     */
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

}