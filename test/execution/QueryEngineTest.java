package execution;

import access.read.AccessIterator;
import access.write.AccessInserter;
import global.DatabaseManager;
import heap.HeapFile;
import heap.TupleDesc;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import parser.Query;
import parser.WhereArgs;

import static org.junit.Assert.*;

public class QueryEngineTest {
	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    private AccessIterator rows;
    private DatabaseManager dbms;

    @Before
    public void setUp() throws Exception {
    	dbms = new DatabaseManager();
    	
        // Create Test Schema
        TupleDesc studentSchema = new TupleDesc();
        studentSchema.addString("name").addInteger("age").addDouble("speed").addBoolean("male");
        dbms.getCatalog().addSchema(studentSchema, "students");
        
        HeapFile students = dbms.getHeapFile("students");
        try(AccessInserter studentInserter = students.inserter()) {
            for (int i = 0; i < 100; i++) {
                studentInserter.insert("Michael", i, 200.5 + i, true);
            }
        }
        rows = students.iterator();
    }


    @Test
    public void testFilterWhereLess() throws Exception {
        WhereArgs.WhereTuple whereArgument = new WhereArgs.WhereTuple("age", WhereArgs.Comparison.LESS, "10");
        AccessIterator filtered = QueryEngine.filterWhere(rows, whereArgument);
        int total = countRows(filtered);
        assertEquals(10, total);
    }

    @Test
    public void testFilterWhereLessOrEqual() throws Exception {
        WhereArgs.WhereTuple whereArgument = new WhereArgs.WhereTuple("age", WhereArgs.Comparison.LEQ, "10");
        AccessIterator filtered = QueryEngine.filterWhere(rows, whereArgument);
        int total = countRows(filtered);
        assertEquals(11, total);
    }

    @Test
    public void testFilterWhereGreater() throws Exception {
        WhereArgs.WhereTuple whereArgument = new WhereArgs.WhereTuple("age", WhereArgs.Comparison.GREATER, "10");
        AccessIterator filtered = QueryEngine.filterWhere(rows, whereArgument);
        int total = countRows(filtered);
        assertEquals(89, total);
    }

    @Test
    public void testFilterWhereGreaterOrEqual() throws Exception {
        WhereArgs.WhereTuple whereArgument = new WhereArgs.WhereTuple("age", WhereArgs.Comparison.GEQ, "10");
        AccessIterator filtered = QueryEngine.filterWhere(rows, whereArgument);
        int total = countRows(filtered);
        assertEquals(90, total);
    }

// Not tested by PASTA, but feel free to uncomment and use yourself    
//    @Test
//    public void testFilterWhereEqual() throws Exception {
//        WhereArgs.WhereTuple whereArgument = new WhereArgs.WhereTuple("age", WhereArgs.Comparison.EQUAL, "10");
//        AccessIterator filtered = QueryEngine.filterWhere(rows, whereArgument);
//        int total = countRows(filtered);
//        assertEquals(1, total);
//    }
    
    @Test
    public void testFilterWhereNotEqual() throws Exception {
        WhereArgs.WhereTuple whereArgument = new WhereArgs.WhereTuple("age", WhereArgs.Comparison.NOT_EQUAL, "10");
        AccessIterator filtered = QueryEngine.filterWhere(rows, whereArgument);
        int total = countRows(filtered);
        assertEquals(99, total);
    }

    @Test
    public void testExecuteWhereSimple() throws Exception {
        Query query = Query.generateQuery("SELECT name, age FROM students WHERE speed < 209.5");
        AccessIterator results = new QueryEngine(dbms).execute(query);
        for(int i =0; i<=8; ++i) {
        	assertTrue("Expected row \"Michael\", " + i, results.next().rowEquals("Michael", i));
        }
        assertFalse("Too many elements in the iterator", results.hasNext());
    }

    @Test
    public void testExecuteWhereRange() throws Exception {
        Query query = Query.generateQuery("SELECT name, age FROM students WHERE speed < 209.5 AND age > 5");
        AccessIterator results = new QueryEngine(dbms).execute(query);
        for(int i =6; i<=8; ++i) {
        	assertTrue("Expected row \"Michael\", " + i, results.next().rowEquals("Michael", i));
        }
        assertFalse("Too many elements in the iterator", results.hasNext());
    }

    @Test
    public void testExecuteWhereRangeExact() throws Exception {
        Query query = Query.generateQuery("SELECT name, age FROM students WHERE speed <= 209.5 AND age >= 5");
        AccessIterator results = new QueryEngine(dbms).execute(query);
        for(int i =5; i<=9; ++i) {
        	assertTrue("Expected row \"Michael\", " + i, results.next().rowEquals("Michael", i));
        }
        assertFalse("Too many elements in the iterator", results.hasNext());
    }

    @Test
    public void testExecuteWhereRangeInProjectionColumns() throws Exception {
        Query query = Query.generateQuery("SELECT speed, age FROM students WHERE speed <= 209.5 AND age >= 5");
        AccessIterator results = new QueryEngine(dbms).execute(query);
        for(int i =5; i<=9; ++i) {
        	assertTrue("Expected row (" + (200.5+i) + ", " + i + ")", results.next().rowEquals(200.5+i, i));
        }
        assertFalse("Too many elements in the iterator", results.hasNext());
    }

    @Test
    public void testExecuteWhereRangeLimited() throws Exception {
        Query query = Query.generateQuery("SELECT speed, age FROM students WHERE speed <= 209.5 AND age >= 5 LIMIT 2");
        AccessIterator results = new QueryEngine(dbms).execute(query);
        for(int i =5; i<=6; ++i) {
        	assertTrue("Expected row (" + (200.5+i) + ", " + i + ")", results.next().rowEquals(200.5+i, i));
        }
        assertFalse("Too many elements in the iterator", results.hasNext());
    }

    @After
    public void tearDown() throws Exception {
        rows.close();
        dbms.close();
    }

    private static int countRows(AccessIterator rows) {
        int i = 0;
        while(rows.hasNext()) {
            rows.next();
            i++;
        }
        return i;
    }
}