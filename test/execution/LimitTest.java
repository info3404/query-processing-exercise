package execution;

import access.read.AccessIterator;
import access.write.AccessInserter;
import global.DatabaseManager;
import heap.HeapFile;
import heap.TupleDesc;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import static org.junit.Assert.*;

public class LimitTest {
	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    private AccessIterator rows;
    private DatabaseManager dbms;

    @Before
    public void setUp() throws Exception {
    	dbms = new DatabaseManager();
    	
        // Create Test Schema
        TupleDesc studentSchema = new TupleDesc();
        studentSchema.addString("name").addInteger("age").addDouble("speed").addBoolean("male");
        dbms.getCatalog().addSchema(studentSchema, "students");
        HeapFile students = dbms.getHeapFile("students");
        try(AccessInserter studentInserter = students.inserter()) {
            for (int i = 0; i < 100; i++) {
                studentInserter.insert("Michael", i, 200.5 + i, true);
            }
        }
        rows = students.iterator();
    }

    @Test
    public void testHasNext() throws Exception {
        AccessIterator limit = new Limit(rows, 10);
        assertTrue("You are being too strict; denying the first tuple to be accessed", limit.hasNext());
        for(int i = 0; i < 10; i++) limit.next();
        assertFalse("You aren't enforcing the limit", limit.hasNext());
        limit.close();
    }

    @Test
    public void testEmptyIterator() throws Exception {
        // Test empty iterator to start with
        while(rows.hasNext()) {rows.next();}
        AccessIterator limit = new Limit(rows, 10);
        assertFalse("You aren't handling an empty iterator properly", limit.hasNext());
        limit.close();
    }

    @Test
    public void testNotEnough() throws Exception {
        // Test empty iterator to start with
        while(rows.hasNext()) {rows.next();}
        AccessIterator limit = new Limit(rows, 101);
        // If this fails, this is because you're only checking the limit,
        // and not if there is still an element in the iterator
        while(limit.hasNext()) limit.next();
        limit.close();
    }

    @Test
    public void testNext() throws Exception {
        final int nRows = 10;
		AccessIterator limit = new Limit(rows, nRows);
        for(int i=0; i<nRows; ++i){
        	assertTrue("Expected row to be (\"Michael\", "+ i + ", " + (200.5+i) +", true)", limit.next().rowEquals("Michael", i, 200.5+i, true));
        }
        limit.close();
    }
}