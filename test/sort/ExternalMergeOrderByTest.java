package sort;

import access.read.AccessIterator;
import access.write.AccessInserter;
import buffer.BufferManager.BufferAccessException;
import global.DatabaseManager;
import heap.HeapFile;
import heap.HeapPage;
import heap.Tuple;
import heap.TupleDesc;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.List;

import static global.DatabaseConstants.MERGE_SORT_BUFFER_FRAMES;
import static org.junit.Assert.*;

public class ExternalMergeOrderByTest {
	// Comment this out for debugging
    @Rule
    public Timeout globalTimeout = new Timeout(1000);
    
    private DatabaseManager dbms;

	private TupleDesc studentSchema;

    @Before
    public void setUp() throws Exception {
    	dbms = new DatabaseManager();
    	
        // Create Test Schema
        studentSchema = new TupleDesc();
        studentSchema.addString("name").addInteger("age").addDouble("speed").addBoolean("male");
        dbms.getCatalog().addSchema(studentSchema, "students");
   }

	/**
	 * @return
	 * @throws BufferAccessException
	 */
	private HeapFile addStudents(int numStudents) throws BufferAccessException {
		HeapFile students = dbms.getHeapFile("students");
        try(AccessInserter studentInserter = students.inserter()) {
            for (int i = 0; i < numStudents; i++) {
                studentInserter.insert("Michael", i, Math.random() * 1000, true);
            }
        }
		return students;
	}

    @After
    public void tearDown() throws Exception {
    	dbms.close();
    }

    @Test
    public void testProduceSortedRuns() throws Exception {
        final String[] sortColumns = new String[]{"speed"};
		ExternalMergeOrderBy sorter = new ExternalMergeOrderBy(dbms, sortColumns);
        ColumnComparator columnComparator = new ColumnComparator(sortColumns);

		int nStudents = 100;
		int nPages = (int) Math.ceil(1.0*nStudents/HeapPage.getMaxRecordsOnPage(studentSchema));
		int nRuns = (int) Math.ceil(1.0*nPages/MERGE_SORT_BUFFER_FRAMES);

		HeapFile students = addStudents(nStudents);
	    AccessIterator rows = students.iterator();
        List<HeapFile> sortedRuns = sorter.produceSortedRuns(rows);
	    rows.close();
        assertEquals("Invalid number of runs. You should have ceil(numTuples / tuplesPerBlock) blocks", nRuns, sortedRuns.size());
        int count = 0;
    	for(HeapFile run : sortedRuns) {
            assertSorted(run, columnComparator);
            try(AccessIterator iterator = run.iterator()) {
                count += countRows(iterator);
            }
        }
        assertEquals("You are missing/have to many elements. Check how you're sorting.", nStudents, count);
        
    }

    @Test
    public void testMergeSortedRuns() throws Exception {
        final String[] sortColumns = new String[]{"speed"};
        ColumnComparator columnComparator = new ColumnComparator(sortColumns);
		ExternalMergeOrderBy sorter = new ExternalMergeOrderBy(dbms, sortColumns);

		int nRuns = 2;
		int nPages = MERGE_SORT_BUFFER_FRAMES * nRuns;
		int nStudents = HeapPage.getMaxRecordsOnPage(studentSchema)  * nPages;

		HeapFile students = addStudents(nStudents);
	    AccessIterator rows = students.iterator();
        List<HeapFile> sortedRuns = sorter.produceSortedRuns(rows);	    	
	    rows.close();
        assertEquals("Starting out with right number of sorted runs to merge", nRuns, sortedRuns.size());

        for(int i=0; i<nRuns; ++i) {
	        int n = dbms.getBufferManager().getPageAccesses().size();
	        try(AccessIterator it = sortedRuns.get(i).iterator()) {
	        	while(it.hasNext()) it.next();
	        }
	        n = dbms.getBufferManager().getPageAccesses().size() - n;
	        assertEquals("Sorted run " + i + " should be expected size", MERGE_SORT_BUFFER_FRAMES, n);			
		}
     
        // Only 1 Merge run this time
        int startingPageAccesses = dbms.getBufferManager().getPageAccesses().size();
        sortedRuns = sorter.mergeSortedRuns(sortedRuns);
        
        // We should only read each page of each run once, and create the same number of new pages, but when we create our output file it has an overhead of 1 additional load.
        assertEquals("You aren't implementing External Merge Sort properly", nPages*2+1, dbms.getBufferManager().getPageAccesses().size() - startingPageAccesses);
        
        // Check that it is sorted
        for(HeapFile sortedFile : sortedRuns) {
            assertSorted(sortedFile, columnComparator);
        }
        
        int count = 0;
        try(AccessIterator iterator = sortedRuns.get(0).iterator()) {
            count += countRows(iterator);
        }
        assertEquals("You are missing/have to many elements. Check how you're sorting.", nStudents, count);
    }

    @Test
    public void testMergeSortedRunsComplex() throws Exception {
        // Add more records to make it more complex
    	int nStudents = 1100;
    	HeapFile students = addStudents(nStudents);
        
        final String[] sortColumns = new String[]{"speed", "age"};
		ExternalMergeOrderBy sorter = new ExternalMergeOrderBy(dbms, sortColumns);

	    AccessIterator rows = students.iterator();
	    AccessIterator sortedRows = sorter.getSortedRows(rows);
	    rows.close();

        int count = countRows(sortedRows);
        sortedRows.close();
        assertEquals("You are missing/have to many elements. Check how you're sorting.", nStudents, count);
    }


    private static void assertSorted(HeapFile file, ColumnComparator columnComparator) throws BufferAccessException {
        try(AccessIterator iterator = file.iterator()) {
            assertTrue("Not enough elements in iterator", iterator.hasNext());
            Tuple previous = iterator.next();
            while(iterator.hasNext()) {
                assertTrue("Elements are NOT sorted", columnComparator.compare(previous, iterator.next()) <= 0);
            }
        }
    }

    private static int countRows(AccessIterator rows) {
        int i = 0;
        while(rows.hasNext()) {
            Tuple row = rows.next();
            i++;
        }
        return i;
    }
}