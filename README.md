# Query Processing and Sorting #
This week looks at Query Processing and the concepts of pipelining and materialisation. Through these exercises, we hope you come away with an appreciate and understanding of why databases use pipelining and why it is more efficient that materialisation in most cases.

## Extra Details for the Scaffold Code
The **query engine** is what converts the text "SELECT name FROM students", into the commands the database can understand, e.g. load table students, project over the name of each student, print out the results. Each of the filters you add in filterWhere, are a way of chaining iterators. This is essentially how this Database pipelines its data. Reading one row at a time, it checks if a condition is true. If it is the row is returned, otherwise the next row is looked at.
**filterWhere** takes an iterator and a where clause as its arguments. Using this, it determines what comparison is needed (e.g. greater than, less than) and applies it to the iterator and returns it (pipelining!!)
**execute** runs a query, and pieces all of the different stages of the pipeline together. e.g. applying a projection or a filter to the iterator
The **limit** operator does exactly what you expect. Taking a number X, it will return the first X rows from an iterator before it will stop.

## Easy
For this task, you are expected to:

- Finish the implementation of the WHERE clause in our query parser. Have a look at `QueryEngine`, specifically the `filterWhere` and `execute` methods
- Implement the LIMIT operator that can be found in the `execution` package.
- Pass the `LimitTest` and `QueryEngineTest` unit tests 

What you are doing here, is actually implementing the WHERE and LIMIT clauses in the database. We strongly encourage to try running the `Main.java` file and entering in the commands to try out each of these keywords once you have implemented them. 
 
## Medium
For this task, you are expected to:

- Implement the Easy task
- Implement the `InMemoryOrderBy` class in the `sort` package. This asks you to materialise the rows and sort them in memory
- Pass the `InMemoryOrderByTest` unit tests

This involves modifying `InMemoryOrderBy` so you are sorting the rows in memory, and filling out the methods for the `AccessIterator` so the database can access this result one row at a time.
__Hint: Column Comparator may be useful__
 
## Hard
For this task, you are expected to:

- Implement the Easy task
- Implement the `ExternalMergeOrderBy` in the `sort` package. This asks you to implement the External Merge Sort algorithm covered in the lectures
- Pass the `ExternalMergeOrderByTest` unit tests

This involves modifying `ExternalMergeOrderBy` so you are sorting the rows as per the External Merge Sort algorithm. Each method in the class is well documented so it's purpose should appear clear to you. If you are still unsure what is going on don't hesitate to post a question to the discussion forum.
__Hint: Column Comparator may be useful __
 
**Note:** If you want to test `ExternalMergeOrderBy` in your command line query parser, you will need to change the line in `QueryEngine` from using the `InMemoryOrderBy`, to `ExternalMemoryOrderBy` class appropriately. You do not need to do that to pass the tests

This time hopefully you all should have a bit more fun with the code since you can execute queries and see the result. If you run the Main method, it will prompt you for input and an example query you can run would be `SELECT name, age FROM students`. As you implement, LIMIT, ORDER BY and WHERE you can actually see the results in the queries that you do.